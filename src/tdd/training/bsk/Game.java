package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frameArray;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	private final static int GAME_FRAMES=10;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frameArray=new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frameArray.size()<GAME_FRAMES) {
			frameArray.add(frame);
		}else {
			throw new BowlingException();
		}		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frameArray.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if(firstBonusThrow<0 || firstBonusThrow>Frame.MAX_FRAME_SCORE || frameArray.size()!=GAME_FRAMES || !frameArray.get(GAME_FRAMES-1).isSpare()) {
			throw new BowlingException();
		}
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if(secondBonusThrow<0 || secondBonusThrow>10 || frameArray.size()!=GAME_FRAMES || !frameArray.get(GAME_FRAMES-1).isStrike()) {
			throw new BowlingException();
		}
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score=0;
		for(int i=0; i<frameArray.size(); i++) {
			if(frameArray.get(i).isStrike() && i<frameArray.size()-1) {
				if(frameArray.get(i+1).isStrike()) {
					if(i+1==GAME_FRAMES-1) {
						frameArray.get(i).setBonus(frameArray.get(i+1).getFirstThrow()+firstBonusThrow);
					}else {
						int frame2FirstThrow;
						try {
							frame2FirstThrow=frameArray.get(i+2).getFirstThrow();
						}catch (Throwable e) {
							frame2FirstThrow=0;
						}
						frameArray.get(i).setBonus(frameArray.get(i+1).getFirstThrow()+frame2FirstThrow);
					}
				}else {
					frameArray.get(i).setBonus(frameArray.get(i+1).getFirstThrow()+frameArray.get(i+1).getSecondThrow());
				}
			}else if(frameArray.get(i).isSpare() && i<frameArray.size()-1) {
				frameArray.get(i).setBonus(frameArray.get(i+1).getFirstThrow());
			} 
			score+=frameArray.get(i).getScore();
		}
		
		return score+firstBonusThrow+secondBonusThrow;
	}

}
