package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test
	public void testInputFirstThrow() throws Exception{
		assertEquals(new Frame(1,5).getFirstThrow(), 1);
	}
	
	@Test
	public void testInputSecondThrow() throws Exception{
		assertEquals(new Frame(1,5).getSecondThrow(), 5);
	}
	
	@Test(expected=BowlingException.class)
	public void testInputThrowsSumIsLesserEqualTo10() throws Exception{
		new Frame(10,5);
	}
	
	@Test(expected=BowlingException.class)
	public void testInputFirstThrowIsGreaterEqualTo0() throws Exception{
		new Frame(-1,5);
	}
	
	@Test(expected=BowlingException.class)
	public void testInputSecondThrowIsGreaterEqualTo0() throws Exception{
		new Frame(1,-5);
	}

	@Test
	public void testOrdinaryFrameScoreShouldBe8() throws Exception{
		assertEquals(new Frame(2,6).getScore(), 8);
	}
	
	@Test
	public void testBonusShouldBe8() throws Exception{
		Frame frame=new Frame(2,6);
		frame.setBonus(8);
		assertEquals(frame.getBonus(), 8);
	}
	
	@Test(expected=BowlingException.class)
	public void testBonusShouldBeGreaterEqualTo0() throws Exception{
		new Frame(2,6).setBonus(-1);
	}
	
	@Test
	public void testFrameIsSpare() throws Exception{
		assertEquals(new Frame(1,9).isSpare(), true);
	}
	
	@Test
	public void testFrameIsNotSpare() throws Exception{
		assertEquals(new Frame(1,8).isSpare(), false);
	}
	
	@Test
	public void testSpareFrameScoreShouldBe13() throws Exception{
		Frame frame=new Frame(1,9);
		frame.setBonus(3);
		assertEquals(frame.getScore(), 13);
	}
	
	@Test
	public void testFrameIsStrike() throws Exception{
		assertEquals(new Frame(10,0).isStrike(), true);
	}
	
	@Test
	public void testFrameIsNotStrike() throws Exception{
		assertEquals(new Frame(9,0).isStrike(), false);
	}
	
	@Test
	public void testStrikeFrameScoreShouldBe19() throws Exception{
		Frame frame=new Frame(1,9);
		frame.setBonus(9);
		assertEquals(frame.getScore(), 19);
	}
}
