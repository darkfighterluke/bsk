package bsk;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	@Test
	public void testAddFrameToGame() throws Exception{
		Game game=new Game();
		Frame frame=new Frame(1,5);
		game.addFrame(frame);
		assertEquals(game.getFrameAt(0), frame);
	}
	
	@Test(expected=BowlingException.class)
	public void testShouldNotAddMoreThan10FramesToGame() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		Frame frame11=new Frame(1,5);
		game.addFrame(frame11);
	}
	
	@Test
	public void testFrame1ShouldBeTheSecondAdded() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		assertEquals(game.getFrameAt(1), frame2);
	}
	
	@Test
	public void testGameScoreShouldBe81() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 81);
	}
	
	@Test
	public void testGameScoreWithSpareFrameShouldBe88() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,9);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 88);
	}
	
	@Test
	public void testGameScoreShouldBe90WithLastFrameSpare() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,9);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(4,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 90);
	}
	
	@Test
	public void testGameScoreWithStrikeFrameShouldBe94() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 94);
	}
	
	@Test
	public void testGameScoreWithConsecutiveStrikeAndSpareShouldBe103() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2=new Frame(4,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 103);
	}
	
	@Test
	public void testGameScoreWithConsecutiveStrikesShouldBe112() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2=new Frame(10,0);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 112);
	}
	
	@Test
	public void testGameScoreWithConsecutiveSparesShouldBe98() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(8,2);
		game.addFrame(frame1);
		Frame frame2=new Frame(5,5);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,6);
		game.addFrame(frame10);
		assertEquals(game.calculateScore(), 98);
	}
	
	@Test
	public void testFirstBonusShouldBe7() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		assertEquals(game.getFirstBonusThrow(), 7);
	}
	
	@Test(expected=BowlingException.class)
	public void testFirstBonusThrowShouldBeGreaterEqualTo0() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(-1);
	}
	
	@Test(expected=BowlingException.class)
	public void testFirstBonusThrowShouldBeLesserEqualTo10() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(11);
	}
	
	@Test
	public void testGameScoreWithFirstBonusThrowShouldBe90() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(2,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		assertEquals(game.calculateScore(), 90);
	}
	
	@Test(expected=BowlingException.class)
	public void testFirstBonusThrowWithoutLastSpareShouldThrowException() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(1,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
	}
	
	@Test
	public void testSecondBonusThrowShouldBe3() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(3);
		assertEquals(game.getSecondBonusThrow(), 3);
	}
	
	@Test(expected=BowlingException.class)
	public void testSecondBonusThrowShouldBeGreaterEqualTo0() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(-1);
	}
	
	@Test(expected=BowlingException.class)
	public void testSecondBonusThrowShouldBeLesserEqualTo10() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(11);
	}
	
	@Test(expected=BowlingException.class)
	public void testSecondBonusThrowWithoutLastStrikeShouldThrowException() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(9,1);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(3);
	}
	
	@Test
	public void testGameScoreWithSecondBonusThrowShouldBe92() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2=new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3=new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4=new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5=new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6=new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7=new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8=new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9=new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10=new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(game.calculateScore(), 92);
	}
	
	@Test
	public void testGamePerfectScoreShouldBe300() throws Exception{
		Game game=new Game();
		Frame frame1=new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2=new Frame(10,0);
		game.addFrame(frame2);
		Frame frame3=new Frame(10,0);
		game.addFrame(frame3);
		Frame frame4=new Frame(10,0);
		game.addFrame(frame4);
		Frame frame5=new Frame(10,0);
		game.addFrame(frame5);
		Frame frame6=new Frame(10,0);
		game.addFrame(frame6);
		Frame frame7=new Frame(10,0);
		game.addFrame(frame7);
		Frame frame8=new Frame(10,0);
		game.addFrame(frame8);
		Frame frame9=new Frame(10,0);
		game.addFrame(frame9);
		Frame frame10=new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(game.calculateScore(), 300);
	}
}
